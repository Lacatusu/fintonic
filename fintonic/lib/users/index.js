'use strict';

const bcrypt                = require('bcryptjs');
const User                  = require('../../models/users');
const validateLoginInput    = require('../validator/login');
const jwt                   = require('jsonwebtoken');
const logger                = require('../logger').info;

//Registro un usuario nuevo en la bbdd
const registerUsers         =  (req, res)=> {

  User.findOne({ email: req.body.email }).then(user => {
    if (user) {
      const error = 'Email already exists';
      return res.status(400).json(error);
    } else {

      const newUser = new User({
        firstName         : req.body.firstName,
        lastName          : req.body.lastName,
        email             : req.body.email,
        password          : req.body.password
      });
      //Encripto la contraseña
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
          if (err) throw err;
          newUser.password = hash;
          logger.info('Succes creating user');
          newUser
            .save()
            .then(user => res.json(user))
            .catch(err => logger.error('Error creating user'))
        });
      });
    }
  });
}

/*Funcionalidad para el logueo de usuario
 * Si el usuario se ha logueado correctamente recibe
 * un jwt firmado con la clave secreta
*/
const loginUsers = (req, res)=> {
  const { errors, isValid } = validateLoginInput(req.body);
  
  // Chequeo que las cabeceras vengan correctamente
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const email = req.body.email;
  const password = req.body.password;

  // Busco usuario por email para ver si existe 
  User.findOne({ email }).then(user => {
    if (!user) {
      errors.email = 'User not found';
      return res.status(404).json(errors);
    }

    // Comparo la contraseña que mete el usuario con la que hay en la BBDD
    bcrypt.compare(password, user.password).then(isMatch => {
      if (isMatch) {
        // Usuario logueado correctamente
        const payload = { id: user.id,
        firstName: user.firstName,
        lastName: user.lastName,
       }; // Declaro la clave con la que se firmara el jwt
        const secret  = "khiukb5#0im";
        // Firmo el token con el payload personalizado
        const jwtBearerToken = jwt.sign(
          payload,
          secret,
          { expiresIn: 3600 },
          (err, token) => {
            res.json({
              success: true,
              token: 'Bearer ' + token
            });
          }
        );        
      } else {
        errors.password = 'Password incorrect';
        return res.status(400).json(errors);
      }
    });
  });
}

module.exports = {
  registerUsers,
  loginUsers
}
