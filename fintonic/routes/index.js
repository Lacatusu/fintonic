'use strict'

const express  = require('express');
const users    = require('../lib/users');
const products = require('../lib/products');
const router   = express.Router();
const passport = require('passport');

  /*----------------------------------------Product Api Request OPTIONS---*/	
  //Get Options
router.get('/products/all', products.getAllProducts);

  //Post Options
router.post('/register', users.registerUsers);
router.post('/login', users.loginUsers);
router.post('/createProduct',passport.authenticate('jwt', { session: false }), products.registerProduct);
router.post('/deleteProduct',passport.authenticate('jwt', { session: false }), products.deleteProduct);

module.exports = router;